import {defineStore} from "pinia";

export const useSongData = defineStore('songData', {
    state: () => ({
        data: [] as SongInfo[],
        searchData: ''
    }),

    actions: {
        async getData(searchData: any) {
            try {
                this.searchData = searchData
                const searchValue = searchData.value.split(' ').join('+')
                const response  = await fetch('https://itunes.apple.com/search?term=' + searchValue + '&entiti=music').then(res => res.json())
                this.data = response.results
            } catch (error) {
                return error
            }
        }
    }
})

interface SongInfo {
    wrapperType: '',
    kind: '',
    artistId: 0,
    collectionId: 0,
    trackId: 0,
    artistName: '',
    collectionName: '',
    trackName: '',
    collectionCensoredName: '',
    trackCensoredName: '',
    artistViewUrl: '',
    collectionViewUrl: '',
    trackViewUrl: '',
    previewUrl: '',
    artworkUrl30: '',
    artworkUrl60: '',
    artworkUrl100: '',
    releaseDate: '',
    collectionExplicitness: '',
    trackExplicitness: '',
    discCount: 0,
    discNumber: 0,
    trackCount: 0,
    trackNumber: 0,
    trackTimeMillis: 0,
    trackPrice: 0,
    country: '',
    currency: '',
    primaryGenreName: '',
    contentAdvisoryRating: '',
    isStreamable: false,
}